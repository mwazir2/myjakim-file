<?php

    use Carbon\Carbon;
    use App\Like;
    use App\Comment;
    use App\User;
    
    $x_html = file_get_contents("http://islam.gov.my/");
    //
    preg_match_all('#<label style="margin-top:78px;font-size:10px;margin-left:165px;">(.+?)</label>#is', $x_html, $items);
    //print_r($items);
    $time = $items[1][0];
    //die();
?>
<!DOCTYPE html>
<!--

                                 `-://///////::-`                                
                         `-/////:-.`        `.-://///-`                         
                     `///:.       `d:          y:-.  ./+//`                     
                  -/+:`           o:h-         +h.       `///.                  
               ./+-` s+          .::/++++//::.`:-           .:+/`               
             -+/`    `y+-- ./oydNNMMMMNssNMMMNNNdy+:.      y/: `//.             
           -+:`       `-/ymNMMMMMMMMNo`  .sNMMMMMMMMNds:``/+o    `:+.           
         .+:`       `/hNMMMMMMMMMMNo`      .sNMMMMMMMMMMNy/`       `//`         
        /+`./-.   .sNMMMMMMMMMMMNo.          .sNMMMMMMMMMMMm+`       `+-        
      `o-   /o+-.ymyyyyyyyyyyyyo.              .shhhhhhhhhhhhmo`    :+`:/       
     .o.     :`oNMm         .                                MMm/ /s/`  -+`     
    .o`      .hMMMm        .m+                               NMMNy.`     .o`    
   `s`      -mMMMMd        -MM:           :`                 NMMMMd.      .o    
   s.`     :NMMMMMm         Nd /-`       .No                 MMMMMMd.      -+   
  /: ss+/`.NMMMMMMd         dm NNmo.     oMm                 MMMMMMMd`      o-  
 `s /+oo-`dMMMMMMMh         yM`omMMm+    mdN                 NMMMMMMMo -+o+``s  
 /: -:-:`/MMMMMMNs.         oM- .-:yN+  -N+M`                :hMMMMMMN.-s:`  o. 
 s       dMMMMNs.  .        /M+     +m  hy/M.   ./oo/:.        :hMMMMMo      .+ 
 s      .MMMNs.   .s        .Mo      y.sN-:M/ `sdddmNMmho:-..`  `:hMMMd       s 
.o   `  :MNs.     h/        `No      /hMo .Nm:y+.``./smMMMNNd.    `:hMN   `   y 
.+ `sy` /My`      Ns.     `.+m.   .:yNN+   oNMNysyhdmmdyo+//-       .dM` :h+  y 
.o  ..  -MMd/`    yMmhyyyhdmy.`-/ydmho.     :shddys/-.`           `+mMN  `-.  y 
 s      `NMMMd/`  `:shddhs/-./+o+:-.                            `+mMMMh       s 
 s       hMMMMMd/`          -:-:  `-  `+    `.  ./            `+mMMMMM+      -/ 
 /:      :MMMMMMMd:         /s+s `:h- `d`` `/h. :y``         /dMMMMMMm`      o` 
 `s       yMMMMMMMh         ...- ```-  :`. . -. `-``         MMMMMMMM/      .o  
  //      `dMMMMMMh                                          MMMMMMMs       o`  
   o.      .mMMMMMh                                          MMMMMMy       /-   
   `o`      .hMMMMy                                         `MMMMMs`      :/    
    `o`      `sNMMs                                         `MMMN/       :+     
     `+.       :dMy````````````                  ```````````.MNy.       //      
       +:       `+ddmmddddddddds.             `:hddddddddddddh:       `+-       
        :+`       `/dNMMMMMMMMMMNs.         `:hMMMMMMMMMMMNy:        -+`        
         `/:`        -omNMMMMMMMMMNs.     `:dMMMMMMMMMMNh+.        .+:          
           `/:`         -+hNNMMMMMMMNs. `/dMMMMMMMMNms/.         .+:            
             `//`        `. ./oydmNNNNNsdMNNNNmhs+:.          `:+:              
                :/:`     +`       `-.---:--.+ :     /s`    `-+/.                
                  `://.  -/++:.`.-`.:+`     + .+-///-`  `:+/-                   
                      -///:-//-::.-//-o/////:  `.` `-///:`                      
                          `-//////+://-....-://////:.                           
                                  `.-------.`                                   


-->
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MyJAKIM</title>

        <!-- Vendor CSS -->
        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="css/app_1.min.css" rel="stylesheet">
        <link href="css/app_2.min.css" rel="stylesheet">
        <style type="text/css">
        .nospace {
            padding-left: 0px !important;
            padding-right: 0px !important;
            margin: 0 !important;

            vertical-align: text-top;   
            overflow-y: scroll;
            height: 100%;

        }
        .card {
            margin-bottom: 10px;
        }

        .sidebar-left {
            left:0px;
            width:80%;
            padding:20px;
          
            padding-top:5px;
            height:100%;

            background-color:#eee;
            padding-bottom:0;
            padding-left: 5px;
            padding-right: 5px;
        }

        .small-body {
            width: 80%;
        }

        .large-body {
            width: 100%;
        }

        .got-sidebar {
            display: block;
        }

        .no-sidebar {
            display:none;
        }

        .nospace .container {
            padding: 0px;
            padding-left: 5px;
            padding-right: 5px;
            overflow-x: hidden;
        }

        .click-handle {
            cursor: pointer;
            cursor: hand;
        }
        .page-click-handle {
            cursor: pointer;
            cursor: hand;
        }
        #trigger-sidebar {
            cursor: pointer;
            cursor: hand;
        }
        .event-click {
            cursor: pointer;
            cursor: hand;
        }
        .current-page {
            background-color: #ccc !important;
        }
       .hide-when-mobile {
            display:block;
        }

        @media screen and (max-width: 768px) {
            #left-side {
                width: 100%;
            }
            .hide-when-mobile {
                display: none;
            }
        }

        .linky {
            color: #5E5E5E;
        }

        .linky:hover {
            color: #5E5E5E;
        }

        </style>
    </head>

    <body>
        <header id="header" class="clearfix" data-ma-theme="blue">
            <ul class="h-inner" style="padding-top:0px">
                <li id="trigger-sidebar" data-ma-action="sidebar-open" data-ma-target="#sidebar" style="padding-top:10px">
                    <img src="/img/logo-jata.png" style="width:50px;margin-top: 5px"/>
                </li>

                <li class="hi-logo" style="padding-top:25px;padding-left:10px">
                    <a href="/" style="padding:0">Info@ISLAM</a>
                </li>

                <li class="pull-right">
                    <ul class="hi-menu">
<!-- 
                        <li data-ma-action="search-open">
                            <a href=""><i class="him-icon zmdi zmdi-search"></i></a>
                        </li> -->
                         <li class="dropdown hidden-xs" style="padding-top:20px;padding-right: 150px" data-ma-action="search-open">
                            <a href=""><h4 style="color:#fff"><i class="him-icon zmdi zmdi-search"></i> Carian</h4></a>
                        </li>

                        <li class="dropdown hidden-xs" style="padding-top:20px;">
                            <a data-toggle="dropdown" href=""><h4 style="color:#fff">Rujukan Hukum</h4></a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="http://muftiwp.gov.my/index.php/ms-my/perkhidmatan/bayan-linnas" target="_blank">Bayan Linnas</a>
                                </li>
                                <li>
                                    <a href="http://muftiwp.gov.my/index.php/ms-my/perkhidmatan/irsyad-fatwa" target="_blank">Irsyad Fatwa</a>
                                </li>
                                <li>
                                    <a href="http://muftiwp.gov.my/index.php/ms-my/perkhidmatan/irsyad-al-hadith" target="_blank">Irsyad Al-Hadith</a>
                                </li>
                                <li>
                                    <a href="http://muftiwp.gov.my/index.php/ms-my/perkhidmatan/irsyad-usul-fiqh" target="_blank">Irsyad Usul Fiqh</a>
                                </li>
                                <li>
                                    <a href="http://muftiwp.gov.my/index.php/ms-my/perkhidmatan/al-kafili-al-fatawi" target="_blank">Al Kafi Li Al-Fatawi</a>
                                </li>
                            </ul>
                        </li>
                        @if (!$user = Auth::user())
                        <li class="dropdown hidden-xs" style="padding-top:20px;padding-left: 70px">
                            <a href="/login" data-toggle="tooltip" data-placement="bottom" title="Menggunakan Facebook"><h4 style="color:#fff">Log masuk</h4></a>
                        </li>
                        @else
                         <li class="dropdown hidden-xs" style="padding-top:20px;padding-left: 70px">
                            <a href="/logout"><h4 style="color:#fff">{{ $user->name }}</h4></a>
                        </li>

                        @endif
                        <li class="dropdown hidden-xs" style="padding-top:20px;padding-left: 70px">
                            <?php Carbon::setLocale('my'); ?>
                            <a data-toggle="dropdown" href=""><h4 style="color:#fff">{{ $time }}</h4></a>
                        </li>

                         <li class="dropdown hidden-xs" style="padding-top:20px;padding-left: 70px">
                            <a class="info_popup"><h4 style="color:#fff"><span class="glyphicon glyphicon-info-sign"></span></h4></a>
                        </li>


                        <li class="dropdown visible-xs" style="padding-top:20px;">
                            <a data-toggle="dropdown" href="">
                                <div class="line-wrap">
                                    <div class="line top"></div>
                                    <div class="line center"></div>
                                    <div class="line bottom"></div>
                                </div>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:m_post();">Terkini</a>
                                </li>
                                <li>
                                    <a href="javascript:m_event();">Aktiviti</a>
                                </li>
                                <li>
                                    <a href="javascript:m_page();">Agensi</a>
                                </li>
                                <li role="separator" class="divider"></li>
                               <li>
                                    <a href="http://muftiwp.gov.my/index.php/ms-my/perkhidmatan/bayan-linnas" target="_blank">Bayan Linnas</a>
                                </li>
                                <li>
                                    <a href="http://muftiwp.gov.my/index.php/ms-my/perkhidmatan/irsyad-fatwa" target="_blank">Irsyad Fatwa</a>
                                </li>
                                <li>
                                    <a href="http://muftiwp.gov.my/index.php/ms-my/perkhidmatan/irsyad-al-hadith" target="_blank">Irsyad Al-Hadith</a>
                                </li>
                                <li>
                                    <a href="http://muftiwp.gov.my/index.php/ms-my/perkhidmatan/irsyad-usul-fiqh" target="_blank">Irsyad Usul Fiqh</a>
                                </li>
                                <li>
                                    <a href="http://muftiwp.gov.my/index.php/ms-my/perkhidmatan/al-kafili-al-fatawi" target="_blank">Al Kafi Li Al-Fatawi</a>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="" data-ma-action="search-open"><i class="him-icon zmdi zmdi-search"></i> Carian...</a>
                                </li>
                            </ul>
                        </li>

                   

                    </ul>
                </li>
            </ul>

            <!-- Top Search Content -->
            <div class="h-search-wrap">
                <div class="hsw-inner">
                    <i class="hsw-close zmdi zmdi-arrow-left" data-ma-action="search-close"></i>
                    <form method="get">
                        {{ csrf_field() }}
                        <input type="text" name="q">
                    </form>
                </div>
            </div>

        </header>
        <section id="main"  style="padding-top:45px;padding-right:0px;width:100%;height:100%;-webkit-overflow-scrolling: touch;">

<!--             <aside style="left:0px;width:250px;" class="sidebar">
                <p>Profile list</p>
            </aside>
            <aside style="left:260px;width:300px;" class="sidebar">
                <p>Profile list</p>
            </aside>
            <aside style="left:570px;width:470px;" class="sidebar">
                <p>Profile list</p>
            </aside> -->
            <aside class="sidebar sidebar-left small-body" id="left-side">
            <div class="row" style="width:100%;height:100%;margin-right:0;margin-left:0;overflow: hidden;padding-bottom: 70px">

                <div class="partition col-sm-3 nospace hide-when-mobile" id="event-latest">
                    <div class="container" style="height:100%">
                    @foreach ($events as $event)
                        <div class="card event-click" event-id="{{ $event->facebook_event_id }}">
                            <div class="card-body" style="">
                                <div class="list-group">
                                    <?php
                                        // get date

                                        $date = Carbon::parse($event->start_time);
                                        Carbon::setLocale('ms');
                                        $day = $date->format('d');
                                        $month = $date->format('F');
                                    ?>
                                    <div class="list-group-item media" style="padding:0px">
                                        <div class="pull-left" style="padding:10px">
                                            <div class="event-date bgm-green">
                                                <span class="ed-day">{{ $day }}</span>
                                                <span class="ed-month-time">{{ $month }}<br/><br/>
                                            {{ $date->format('h:i A') }}</span>
                                            </div>
                                        </div>
                                        <div class="media-body" style="padding:10px 2px;padding-bottom: 0">

                                            <div><strong>{{ $event->name }}</strong><br/>
                                            <small>
                                            @if ($event->place != "")
                                            <?php $event_place = json_decode($event->place); ?>
                                            {{ $event_place->name }}
                                            @endif</small></div>

                                        </div>
                                        <br/>
                                        <img src="{{ $event->facebook_full_picture }}" style="width:100%"/>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
 
                    </div>
                </div>

                <div class="partition col-sm-4 nospace hide-when-mobile" id="page-latest">
                    <div class="container">
                        <form method="post" id="change_page">
                            {{ csrf_field() }}
                            <input type="hidden" name="current_page_id" id="current_page_id" value="0"/>
                        @foreach ($pages_latest as $page)
                        <div class="card click-handle{{ (($current_page_id == $page->page_id) ? ' current-page' : '') }}" page-id="{{ $page->page_id }}">
                            <div class="card-body" style="padding:15px">
         
                                <table style="width:100%">
                                    <tr>
                                        <td style="width: 70px">
                                            <img src="https://graph.facebook.com/{{ $page->facebook_page_id }}/picture" style="border-radius: 100%;max-width:50px"/>
                                        </td>
                                        <td>
                                            <?php Carbon::setLocale('ms'); ?>
                                            <strong>{{ $page->page_name }}<br/><small><?php echo Carbon::createFromTimeStamp(strtotime($page->posted_at))->diffForHumans() ?></small></strong>
                                        </td>
                                    </tr>
                                </table>

                                <br/>
                                {{ substr($page->content, 0, 140) }}... <a href="#">Selanjutnya</a>
                            </div>
                        </div>
                        @endforeach
                        </form>
                    </div>
                </div>
                <div class="partition col-sm-5 nospace" id="post-latest">
                    <div class="container" id="post-latest-container" style="-webkit-overflow-scrolling: touch;">
                        @if ($q)
                        <div class="card">
                            <div class="card-body" style="padding:15px">
                                Hasil carian untuk "{{ $q }}".
                            </div>
                        </div>
                        @endif
                        @foreach ($posts as $post)
                        <div class="card">
                            <div class="card-body" style="padding:15px">
                                <table style="width:100%">
                                    <tr>
                                        <td style="width: 70px">
                                            <img src="https://graph.facebook.com/{{ $post->page_id }}/picture" style="border-radius: 100%;max-width: 50px"/>
                                        </td>
                                        <td>
                                            <?php Carbon::setLocale('ms'); 

                                            ?>
                                            <strong><a href="https://www.facebook.com/{{ $post->post_id }}" target="_blank" class="linky">{{ $post->page_name}}</a><br/><small><?php echo Carbon::createFromTimeStamp(strtotime($post->posted_at))->diffForHumans() ?></small></strong>
                                        </td>
                                    </tr>
                                </table>
                                <br/>

                                <?php echo nl2br($post->post_content); 
                                $count_likes = Like::where('post_id', $post->id)->count();
                                $count_comments = Comment::where('post_id', $post->id)->count();
                                $comments = Comment::where('post_id', $post->id)->orderBy('id', 'desc')->limit(2)->get();
                                ?>
                            </div>
                            @if ($post->picture != '') 
                            <img src="{{ $post->picture }}" style="width:100%"/>
                            @endif
                            <div style="padding: 10px;">
                                <a pid="{{$post->id}}" class="like-btn btn btn-danger btn-xs"><span class="glyphicon glyphicon-heart"></span> <font id="count{{$post->id}}">{{ $count_likes }}</font></a>  <a pid="{{$post->id}}" class="comment-btn btn btn-info btn-xs"><span class="glyphicon glyphicon-comment"></span> <font id="countc{{$post->id}}">{{ $count_comments }}</font></a>
                                <br/><br/>
                                <div style="margin-bottom: 5px" id="comment{{$post->id}}">
                                @foreach ($comments as $comment)
                                <?php
                                $userc = User::find($comment->user_id);
                                ?>
                                  <div class="" style="margin-bottom:5px">
                                     <div class="pull-left" style="padding-right:5px">
                                        <img class="lgi-img" style="border-radius:100%;margin: 5px 0;" src="https://graph.facebook.com/{{$userc->facebook_user_id}}/picture" alt="">
                                     </div>
                                     <div class="media-body" style="padding-left:5px;margin-right:15px;padding:5px 10px;">
                                        <strong>{{ $userc->name }}</strong><br/>{{ $comment->text }}
                                     </div>
                                  </div>
                                @endforeach
                                </div>
                                @if ($comments->count() >= 2)
                                <br/>
                                <p><a class="comment-loader" pid="{{ $post->id}}" limit="2">Load more..</a></p>
                                @endif
                                @if ($user)
                                <form method="post" class="post-comment" pid="{{$post->id}}">
                                  <div class="" style="margin-bottom:5px">
                                     <div class="pull-left" style="padding-right:5px">
                                        <img class="lgi-img" style="border-radius:100%;margin: 5px 0;" src="https://graph.facebook.com/{{$user->facebook_user_id}}/picture" alt="">
                                     </div>
                                     <div class="media-body" style="padding-left:5px;margin-right:15px;padding:5px 10px;">
                                        <input type="text" name="comment" id="commentbox{{$post->id}}" class="form-control" style="border-radius:0"/><br/><button type="submit" class="btn btn-xs btn-success pull-right">Comment</button>
                                     </div>
                                  </div>
                                </form>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div id="loading" class="alert" style="border-radius:0;text-align:center;display:none">
                        <p>Loading more posts...</p>
                    </div>
                </div>
            </div>
            </aside>

            <aside style="right:0px;width:20%;" class="sidebar got-sidebar hidden-xs" id="right-side">
<!--                 <ul class="list-group">
                  <li class="list-group-item">
                    <span class="badge">14</span>
                    Cras justo odio
                  </li>
                  <li class="list-group-item">
                    <span class="badge">14</span>
                    Cras justo odio
                  </li>
                  <li class="list-group-item">
                    <span class="badge">14</span>
                    Cras justo odio
                  </li>
                  <li class="list-group-item">
                    <span class="badge">14</span>
                    Cras justo odio
                  </li>
                  <li class="list-group-item" style="padding:15px; padding-top:5px;padding-bottom:0">
                    <table style="width:100%">
                        <tr>
                            <td style="width:50px">
                                <img style="width:40px;border-radius: 100%" src="https://info.malaysia.gov.my/images/profile/353988448005927.jpg"/>
                            </td>
                            <td>
                                Ketua Setiausaha Negara efef e fefef efe 
                            </td>
       
                        </tr>
                    </table>
                    
                  </li>
                </ul> -->
                <form method="post" id="change_page_sidebar">
                {{ csrf_field() }}
                <div class="list-group lg-odd-black">
                
                            
                            <input type="hidden" name="current_page_id_sidebar" id="current_page_id_sidebar" value="0"/>
                    @foreach ($pages as $page)
                   <div class="list-group-item media{{ (($current_page_id == $page->id) ? ' current-page' : '') }}" style="padding:10px">
                      <!--  -->       
                      <div class="pull-left page-click-handle" page-id-sidebar="{{ $page->id }}" style="display: table; overflow: hidden;">
                         <div class="pull-left" style="padding-right:5px">
                            <img class="lgi-img" style="border-radius:100%;margin: 5px 0;" src="https://graph.facebook.com/{{$page->facebook_page_id}}/picture" alt="">
                         </div>
                         <div class="media-body" style="padding-left:5px;margin-right:15px;padding:5px 10px;display: table-cell; vertical-align: middle;">
                            <small>{{ $page->name }}</small>
                         </div>
                      </div>

<!--                       <div class="pull-right" style="position: absolute;top: 0px;right: 0px;">
                         <div class="checkbox pull-left" style="padding-left:0;margin: 25px 0">
                            <label>
                            <input type="checkbox" value="">
                            <i class="input-helper"></i>
                            </label>
                         </div>
                      </div> -->
                   </div>
                   @endforeach
                  
                </div>
         </form>
            </aside>
        </section>

    <div class="modal fade" tabindex="-1" role="dialog" id="login_info">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Tidak log masuk!</h4>
          </div>
          <div class="modal-body">
            <p>Sila log masuk terlebih dahulu untuk menggunakan servis ini.</p>
            <p> <a href="/login" data-toggle="tooltip" data-placement="bottom" title="Menggunakan Facebook" class="btn btn-primary">Log masuk</a></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="info_popup">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Kelebihan Log Masuk</h4>
          </div>
          <div class="modal-body">
<div flex="" layout="column" class="layout-column flex">
    
<div>
<span class="md-body-2">Maklumat Perhubungan Info@Islam</span><br>

 <br>
 <div layout="row" class="layout-row">
    
    <div flex="50" class="flex-50">
        <span class="md-body-2">Alamat</span><br>
        <span class="md-body-1">Jabatan Agama Kemajuan Islam Malaysia</span><br>
        <span class="md-body-1">Blok A dan Blok B,</span><br>
        <span class="md-body-1">Kompleks Islam Putrajaya</span><br>
        <span class="md-body-1">No 23, Jalan Tunku Abdul Rahman, Presint 3</span><br>
        <span class="md-body-1">62100 Putrajaya</span>
    </div>              
    
    <div flex="50" class="flex-50">
        <span class="md-body-2">No. Telefon</span><br>
        <span class="md-body-1">+60 3-8870 7000</span>

        <br><br>

        <span class="md-body-2">E-mel</span><br>
        <span class="md-body-1">info@islam.gov.my</span>
    </div>
</div>



</div>
<br>
<md-divider></md-divider>
<br>
    <div class="my-info-headline md-body-1">
        Info@Islam adalah sebuah platform yang menghubungkan media sosial (facebook/twitter) agensi kerajaan bagi memudahkan rakyat mendapat maklumat semasa berkaitan islam. Selain itu, ia juga adalah platform utama bagi kegunaan rakyat mencari media sosial rasmi agensi islam kerajaan.
    </div>
    <div class="my-info-features-table">
        <table>
            <tbody><tr class="my-info-head">
                <th class="md-body-1">Ciri-ciri</th>
                <th class="md-body-1">Pengguna Berdaftar</th>
                <th class="md-body-1">Pengguna Tidak Berdaftar</th>
            </tr>
            <tr>
                <td class="md-body-1">Post terkini mengikut agensi</td>
                <th class="md-body-1"><i class="material-icons">done</i></th>
                <th class="md-body-1"><i class="material-icons">done</i></th>
            </tr>
            <tr class="my-info-zebra">
                <td class="md-body-1">Aktiviti terkini</td>
                <th class="md-body-1"><i class="material-icons">done</i></th>
                <th class="md-body-1"><i class="material-icons">done</i></th>
            </tr>
            <tr>
                <td class="md-body-1">Senarai agensi</td>
                <th class="md-body-1"><i class="material-icons">done</i></th>
                <th class="md-body-1"><i class="material-icons">done</i></th>
            </tr>
            <tr class="my-info-zebra">
                <td class="md-body-1">Carian post</td>
                <th class="md-body-1"><i class="material-icons">done</i></th>
                <th class="md-body-1"><i class="material-icons">done</i></th>
            </tr>
            <tr>
                <td class="md-body-1">Link ke post asal facebook</td>
                <th class="md-body-1"><i class="material-icons">done</i></th>
                <th class="md-body-1"><i class="material-icons">done</i></th>
            </tr>
            <tr class="my-info-zebra">
                <td class="md-body-1">Link ke aktiviti facebook</td>
                <th class="md-body-1"><i class="material-icons">done</i></th>
                <th class="md-body-1"><i class="material-icons">done</i></th>
            </tr>
            <tr>
                <td class="md-body-1">Set senarai terkini mengikut agensi pilihan</td>
                <th class="md-body-1"><i class="material-icons">done</i></th>
                <th class="md-body-1"><i class="material-icons">clear</i></th>
            </tr>
            <tr class="my-info-zebra">
                <td class="md-body-1">Mengikuti agensi pilihan</td>
                <th class="md-body-1"><i class="material-icons">done</i></th>
                <th class="md-body-1"><i class="material-icons">clear</i></th>
            </tr>
            <tr>
                <td class="md-body-1">Set minat pada post pilihan</td>
                <th class="md-body-1"><i class="material-icons">done</i></th>
                <th class="md-body-1"><i class="material-icons">clear</i></th>
            </tr>
            <tr class="my-info-zebra">
                <td class="md-body-1">Melihat senarai minat post pilihan</td>
                <th class="md-body-1"><i class="material-icons">done</i></th>
                <th class="md-body-1"><i class="material-icons">clear</i></th>
            </tr>
        </tbody></table>
    </div>
</div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
        <!-- Javascript Libraries -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
     
        <script>
            $(function () {
              $('[data-toggle="tooltip"]').tooltip()
            })

            $(".page-click-handle").click(function() {
                var page_id = $(this).attr('page-id-sidebar');
                //alert(page_id);
                $("#current_page_id_sidebar").val(page_id);
                $("#change_page_sidebar").submit();

            });

            $(".click-handle").click(function() {
                var page_id = $(this).attr('page-id');
                //alert(page_id);
                $("#current_page_id").val(page_id);
                $("#change_page").submit();

            });

            function goToByScroll(id){
                $('#page-latest').animate({
                    scrollTop: $("div[page-id='" + id + "'").offset().top - 100
                },
                    'slow');
                $('#right-side').animate({
                    scrollTop: $("div[page-id-sidebar='" + id + "'").offset().top - 100
                },
                    'slow');
            }

            if ($(".current-page")) {
                var page_id = $(".current-page").attr('page-id');
                if (page_id) {
                    goToByScroll(page_id);
                }
            }

            $(".event-click").click(function() {
                var event_id = $(this).attr('event-id');
                //alert(event_id);
                window.open('https://www.facebook.com/events/' + event_id, '_blank');
                window.focus();
            });

            $("#trigger-sidebar").click(function() {
                // check if hide/expand
                var left = $("#left-side");
                var right = $("#right-side");
                if (left.hasClass('large-body')) {
                    // no sidebar, put one
                    left.removeClass('large-body');
                    right.removeClass('no-sidebar');
                    left.addClass('small-body');
                    right.addClass('got-sidebar');
                } else {
                    left.removeClass('small-body');
                    right.removeClass('got-sidebar');
                    left.addClass('large-body');
                    right.addClass('no-sidebar');
                }
            });

            function m_post() {
                $(".partition").addClass('hide-when-mobile');
                $("#post-latest").removeClass('hide-when-mobile');
            }

            function m_event() {
                $(".partition").addClass('hide-when-mobile');
                $("#event-latest").removeClass('hide-when-mobile');
            }

            function m_page() {
                $(".partition").addClass('hide-when-mobile');
                $("#page-latest").removeClass('hide-when-mobile');
            }

            function login() {
                $('#login_info').modal();
            }

            $(".info_popup").click(function() {
                $('#info_popup').modal();
            });

            function ereload() {
                $(".comment-loader").unbind('click');
                $(".post-comment").unbind('submit');
                $(".like-btn").unbind('click');
                
                $(".like-btn").click(function(x) {
                    var el = $(this);
                    var pid = el.attr('pid');
                    $.post("/api/likes", { pid: pid, _token: '{{ csrf_token() }}'})
                      .done(function( data ) {
                        if (data.likes) {
                            $("#count" + pid).html(data.likes);
                        } else {
                            login();
                        }
                      });

                });
                $(".comment-loader").click(function() {
                    var el = $(this);
                    var pid = el.attr('pid');
                    var limit = el.attr('limit');
                    limit = limit * 2;
                    el.html("Loading...");

                    $.get("/api/comments", { pid: pid, _token: '{{ csrf_token() }}', limit: limit})
                      .done(function( data ) {
                        if (data.comments) {
                            $("#comment" + pid).html('');
                            $.each(data.comments, function( key, value ) {
                                var code = '<div class="" style="margin-bottom:5px">\n<div class="pull-left" style="padding-right:5px">\n<img class="lgi-img" style="border-radius:100%;margin:5px 0" src="https://graph.facebook.com/'+value.user.facebook_user_id + '/picture" alt="">\n</div>\n<div class="media-body" style="padding-left:5px;margin-right:15px;padding:5px 10px">\n<strong>' + value.user.name + '</strong><br/>' + value.comment + '\n</div>\n</div>';
                                $("#comment" + pid).append(code);
                            });
                            el.attr('limit', limit);
                            el.html("Load more...");
                        } else {
                            login();
                        }
                      });

                });

                $(".post-comment").submit(function(e) {
                     e.preventDefault();
                    var el = $(this);
                    var pid = el.attr('pid');
                   
                    // submit
               
                    $.post("/api/comments", { pid: pid, _token: '{{ csrf_token() }}', comment: el.serializeArray()[0].value})
                      .done(function( data ) {
                        if (data.comments) {
                            var code = '<div class="" style="margin-bottom:5px">\n<div class="pull-left" style="padding-right:5px">\n<img class="lgi-img" style="border-radius:100%;margin:5px 0" src="https://graph.facebook.com/'+data.user.facebook_user_id + '/picture" alt="">\n</div>\n<div class="media-body" style="padding-left:5px;margin-right:15px;padding:5px 10px">\n<strong>' + data.user.name + '</strong><br/>' + data.comment + '\n</div>\n</div>';
                            $("#comment" + pid).append(code);
                            $("#countc" + pid).html(data.comments);
                            $("#commentbox" + pid).val("");
                        } else {
                            login();
                        }
                      });
                });
            }

            var timestamp = "{{ $timestamp }}";
            var page = 1;
            var token = "{{ csrf_token() }}";
            var more_pages = true;
            var current_page_id = "{{ $current_page_id }}";
            var current_page_id_sidebar = "{{ $current_page_id_sidebar }}";

            ereload();
            $('#post-latest').on('scroll', function() {

                if (!more_pages) {
                    $("#loading").html('<p>No more posts. :(</p>');
                    $("#loading").show();
                } else {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        $("#loading").show();
                        var url = '/?_token=' + token + '&json=1&timestamp=' + timestamp + '&page=' + (page + 1);
                        if (current_page_id != "") url = url + "&current_page_id=" + current_page_id;
                        if (current_page_id_sidebar != "") url = url + "&current_page_id_sidebar=" + current_page_id_sidebar;

                        $.post(url)
                        .done(function(data) {
                            $("#loading").hide();
                            if (data.html) {
                                if (!data.more_pages) {
                                    more_pages = false;
                                }

                                page = data.current_page;
                                $("#post-latest-container").append(data.html);
                                ereload();
                            }
                        });
                    }
                }
            })


    $('body').on('click', '[data-ma-action]', function (e) {
        e.preventDefault();

        var $this = $(this);
        var action = $(this).data('ma-action');

        switch (action) {



            /*-------------------------------------------
                Top Search Open/Close
            ---------------------------------------------*/
            //Open
            case 'search-open':
                $('#header').addClass('search-toggled');
                $('#top-search-wrap input').focus();

                break;

            //Close
            case 'search-close':
                $('#header').removeClass('search-toggled');

                break;
        }
    });

        </script>
    </body>
  </html>
